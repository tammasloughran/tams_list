# Tam's projects list

List of projects I worked on

## BLUE development
### Parallel
- Other people involved: [Raphael Ganzenmueller](mailto:raphael.ganzenmueller@geographie.uni-muenchen.de), [Julia Nabel](mailto:julia.nabel@mpimet.mpg.de)
- Branch [here](https://gitlab.com/bookkeeping_with_BLUE/blue_code/-/tree/parallel)
- Goal: the new HILDA LUC dataset is too large to use for BLUE. While Raphael has run BLUE on small chunks of HILDA for Europe, this is very time consuming and laborious. It would be prefereable to have a version of BLUE that is capable of handling the chunking itsef. Dask and Xarray are typicaly used for this purpose in python so this project replaces all the data structures in BLUE with Xarrays, so that it can be run in parallel.
- To do:
  * Please see the [README.md](https://gitlab.com/bookkeeping_with_BLUE/blue_code/-/blob/parallel/README.md) of this branch.
### Unit testing
- Other people involved: ~~Kersin Hartung~~
- Branch [here](https://gitlab.com/bookkeeping_with_BLUE/blue_code/-/tree/gitlabCI)
- Goal: Unit testing development is not something that is typically done in model development, but it really should be because it promotes robust/reliable software that is less prone to bugs. This branch is for developing unit tests for BLUE, run as both a whole, and testing sub components.
- Done:
  * Some simple tests on the model run as a whole to check reproducability of results.
  * Some Simple tests on a few model components. (area caluclations and get fraction per pft function)
- To do:
  * There is too much to list here and it's a really big job. Basically everything else in the model should be tested somehow. Much of the emissions calculations are simply all contained within the steping function in a god class and do not exist as separate and testable functions (i.e. some refactoring might be needed if you want to test everything individually).
### Documentation updates
- Other people involved: [Selma Bultan](mailto:selma.bultan@geographie.uni-muenchen.de) (helped with new emissions calculations docu.)
- Branch [here](https://gitlab.com/bookkeeping_with_BLUE/blue_code/-/tree/documentation)
- Goal: BLUE is in active development and it's functionality cn sometimes change. This branch is for setting up auto generated documentation from in-line comments, and changing the technical documentation as model function changes.
### Consistency and C conservation checks
- Other people involved: [Selma Bultan](mailto:selma.bultan@geographie.uni-muenchen.de) (first noticed some problems in BLUE)
- Branch [here](https://gitlab.com/bookkeeping_with_BLUE/blue_code/-/tree/consistency)
- Goal: It is important that when blue is run it is capable of ensuring physical contraints are respected. This branch is for developing features that test the consistency of carbon conservation.
- Done:
  * Some simple checks that ensure that C pools do not deplete below 0.
  * A check that the cover fractions add to 1. (In the LUH2 data, the frctions often do not add up)
- To do:
  * Calculations of emissions involve multiplication of numbers that are several orders of magnitude different, and some precision is inevitable lost, leading to occasional negative carbon pools about as large as the loss of precision of those calculations. A solution may be to use python's [decimal](https://docs.python.org/3/library/decimal.html) module to do these calculations in high fixed point precision, then convert back to lower precsion for the rest of the model execution.

## MPI-ESM/JSBACH
### Running MPI-ESM on LRZ
- Repository with instructions [here](https://gitlab.com/tammasloughran/supermuc.patch)
- Goal: Set up MPI-ESM on LRZ SuperMUC.
- Done: /pool data has been tranfered to  Supermuc and MPI-ESM default spin-up simulations can be run using IMDI runscripts.
- To do:
  * Set up other standard experiment configurations (also for mkexp).
  * Copy over other /pool data that are needed by JSBACH standalone simulation.

## Research
### Grand ensemble carbon balance and CBALONE simulations
- Other People involved: [Julia Pongratz](mailto:julia.pongratz@mpimet.mpg.de), [Julia Nabel](mailto:julia.nabel@mpimet.mpg.de), [Hongmei Li](mailto:hongmei.li@mpimet.mpg.de), [Wolfgang Obermeier](mailto:wolfgang.obermeier@geographie.uni-muenchen.de), [Kerstin Hartung](mailto:kerstin.hartung@geographie.uni-muenchen.de), [Ana Bastos](mailto:abastos@bgc-jena.mpg.de), [Felix Havermann](mailto:Felix.Havermann@lmu.de), 
- Repository [here](https://gitlab.com/tammasloughran/grand-ensemble-cbalone)
- Model Repository `mpiesm-landveg_Jan2019`
- HPSS Archive data `/hpss/arch/bm0891/m300719/grand_ensemble_gcb`
- Done: Mostly everything, article submitted and in review.
### Herbaceous Biomass Plantations
- Other people involved: ~~Dorothea~~, [Julia Pongratz](mailto:julia.pongratz@mpimet.mpg.de)
- Wiki [here](https://code.mpimet.mpg.de/projects/fom/wiki/BiomassPlantations_revived)
- Update  of HBP to CMIP6 version of JSBACH. Repository [here](https://gitlab.com/tammasloughran/herbacous_biomass_plantations)
- Paper revisions repository [here](https://gitlab.com/tammasloughran/herbacous_biomass_plantations/-/tree/master/Dorothea_Paper)
- Done: HBP in CMIP6 as implemented by dorothea. And an aditional carbon pool for CDR.
- Todo: Further testing of model configuration and develop some new experiments.
### Ana's Drought 2018/2019 simulations
- Other people involved: [Ana Bastos](mailto:abastos@bgc-jena.mpg.de), and many other modelling groups...
- Repository [here](https://gitlab.com/tammasloughran/drought2018)
- Done: completed

## Teaching
### Advanced data analysis and uncertainty
- Other people involved: [Felix Havermann](mailto:Felix.Havermann@lmu.de), [Ana Bastos](mailto:abastos@bgc-jena.mpg.de), [Julia Pongratz](mailto:julia.pongratz@mpimet.mpg.de)
- Repository [here](https://gitlab.com/tammasloughran/data_uncertainty)
- This course has been taken over by David Gampe for now. Course will be rewritten in R again, so most of this will be redundant.
### Urban heat island excursion.
- Other people involved: [Thomas Mayer](mailto:t.mayer@lmu.de) (for access to instruments)
- This excursion is done mostly independantly by students. The only teaching material is a lecture and some demonstrations.
* Python modeling Berufsvorbereitung
  - Repository [here](https://gitlab.com/tammasloughran/python_modelling)
